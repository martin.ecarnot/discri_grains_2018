datm=prep_data()

# ASCA
ssech=1
datr=as.matrix(datm[seq(1,nrow(datm),ssech),]$x[,seq(1,ncol(datm$x),20)])
factr=sapply(datm[seq(1,nrow(datm),ssech),c(6,8)] , FUN=as.numeric)

# Enleve geno qui n'ont qu'1 spectre dans une condition
i1=which(apply(table(factr[,1], factr[,2]),1,FUN=min) <2)
datr=datr[-which(factr[,1] %in% names(i1)),]
factr=factr[-which(factr[,1] %in% names(i1)),]

opt=asca('options')
opt$permtest="on"
opt$nperm=500
r=asca(snv(datr),factr,opt)
ev=c(r$XA$EffectExplVar,r$XB$EffectExplVar,r$XAB$EffectExplVar)
p=c(r$XA$EffectSignif$p,r$XB$EffectSignif$p,r$XAB$EffectSignif$p)
disp_fac=paste( r$TermLabels[-1],c(colnames(factr), paste(colnames(factr),collapse="x")),sep = "=")
cat(disp_fac)
print(ev)
print(p)
print(unlist(r$SignificantTerms))
